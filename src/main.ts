import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';

platformBrowserDynamic().bootstrapModule(AppModule)
	.then(success => console.log('Aplicação Controle de Vendas no ar'))
	.catch(error => console.log(error));
