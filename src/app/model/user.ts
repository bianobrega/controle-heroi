export class User {
	id: String;
	email: String;
	password: String;
	role: String;
	token: String;
	keepConnected: Boolean = false;
}