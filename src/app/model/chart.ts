export class Chart {
	options:any;
	label:string[];
	type:string;
	legend:boolean = true;
	data:any[];
}