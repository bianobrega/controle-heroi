export class Hero {
	id: String;
	heroNumber: Number;
	name: String;
	avatar: String;
}