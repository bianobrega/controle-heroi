import { Component,
		AfterViewInit } from '@angular/core';
import { Router }        from '@angular/router';
import { UserService }   from './service/user.service';

import { User }			from './model/user';

declare var $: any;

@Component({
  selector: 'my-app',
  templateUrl: './templates/default/main/app-general-template.html',
})
export class AppComponent implements AfterViewInit { 
	public user: User;
    
    constructor(private userService: UserService,
        private router: Router) {

        this.user = userService.getLoggedUser();
        if (this.user == undefined) {
            if(window.location.pathname !== '/login') {
                this.router.navigate(['/login']);
                }
            return;
        }
    }

	ngAfterViewInit() {
        // Init jQuery plugins after the view is loaded
        $('.button-collapse').sideNav();
        $('.nav-wrapper ul a, #internal-menu ul a').unbind("click").click(function(evt: any){
          $('.button-collapse').sideNav('hide');
        });
        $('select').material_select();
        $('.carousel.carousel-slider').carousel(
            { full_width: true, indicators: true }
        );
    }
}
