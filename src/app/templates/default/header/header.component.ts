import { Component,
		OnInit } from '@angular/core';
import { UserService } from '../../../service/user.service';

@Component({
	selector: 'default-header',
	templateUrl: './header.template.html'
})

export class DefaultHeaderComponent implements OnInit{

	constructor(private userService: UserService) { }

	ngOnInit():void {
	}

	logout() {
		this.userService.removeLoggedUser();
		window.location.reload();
	};


};
