export class StatisticUtil {
	public getUrlStatistic(currentStatistic: number, typeStatistic:number): string {
		var url = 'https://demo0385786.mockable.io/api/';

		switch (currentStatistic) {
			case 1:
				if (typeStatistic === 1) {
					url += 'statistic/heroes/month';
				} else {
					url += 'statistic/heroes/year'
				}
			
				break;

			case 2:
				if (typeStatistic === 1) {
					url += 'statistic/general/month';
				} else {
					url += 'statistic/general/year'
				}
				
				break;

			case 3:
				if (typeStatistic === 1) {
					url += 'statistic/general/people/month';
				} else {
					url += 'statistic/general/people/year'
				}
				break;

			case 4:
				if (typeStatistic === 1) {
					url += 'statistic/general/month';
				} else {
					url += 'statistic/general/year'
				}
				break;
			
			default:
				break;
		}
		console.log(url);
		return url;
	}

	public getStatisticInfo(currentStatistic: number, typeStatistic:number): string[] {
		var statisticInfo:string[] = [];

		switch (currentStatistic) {
			case 1:
				statisticInfo.push('bar');
				if (typeStatistic === 1) {
					statisticInfo.push('Números de pessoas salvas no último ano por herói');
				} else {
					statisticInfo.push('Números de pessoas salvas nos últimos anos por herói');
				}
				break;

			case 2:
				statisticInfo.push('line');
				if (typeStatistic === 1) {
					statisticInfo.push('Números de pessoas salvas no último ano');
				} else {
					statisticInfo.push('Números de pessoas salvas nos últimos anos');
				}
				break;

			case 3:
				statisticInfo.push('pie');
				if (typeStatistic === 1) {
					statisticInfo.push('Tipos de pessoas salvas no último ano por idade');
				} else {
					statisticInfo.push('Números de pessoas salvas por idade');
				}

			case 4:
				statisticInfo.push('line');
				if (typeStatistic === 1) {
					statisticInfo.push('Números de pessoas salvas no último ano');
				} else {
					statisticInfo.push('Números de pessoas salvas nos últimos anos');
				}
			default:
				break;
		}

		return statisticInfo;
	}
}