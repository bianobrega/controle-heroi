import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent }   	from './components/login/login.component';
import { StatisticComponent }	from './components/statistic/statistic.component';
import { HeroComponent }		from './components/hero/hero.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login',  component: LoginComponent },
  { path: 'statistic', component: StatisticComponent },
  { path: 'heroes', component: HeroComponent }
];

@NgModule({
  imports: [ 
  	RouterModule.forRoot(routes)
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
