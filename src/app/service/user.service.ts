import { Injectable } 	from '@angular/core';
import { Http,
         Response}		from '@angular/http';

import { Observable }   from 'rxjs';

import { User }			from '../model/user';

import 'rxjs/add/operator/map';

@Injectable()
export class UserService {
	constructor(private http: Http) { }

	login(login: User): Observable<boolean> {
		var url:string = '';
		if (login.role !== 'E') {
			//Utilizada um URL com sucesso do login para testes.
			url = 'https://demo0385786.mockable.io/user/login';
		} else {
			//Utilizada um URL sem sucesso do login para testes.
			url = 'https://demo0385786.mockable.io/user/loginError';
		}
    return this.http.post(url, {
      'userName': login.email,
      'password': login.password
    }).map((response: Response) => {
    	console.log(response)
          var loginResponse = response.json();
          if (!loginResponse || !loginResponse.token) {
              return false;
          }
          login.token = loginResponse.token;

          this.addLoggedUser(login);
          return true;
      });
  };


	addLoggedUser(user: User) {
		if (user.keepConnected) {
			localStorage.setItem('currentUser', JSON.stringify(user));
		} else {
			sessionStorage.setItem('currentUser', JSON.stringify(user));
		}
	};

	getLoggedUser(): User {
		var user: User;
		let userJson = sessionStorage.getItem('currentUser') || localStorage.getItem('currentUser');

		if (userJson) {
			user = JSON.parse(userJson);
		}

		console.log(user);

		return user;
	};

	removeLoggedUser() {
		localStorage.removeItem('currentUser');
		sessionStorage.removeItem('currentUser');
	};
}