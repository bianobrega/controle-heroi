import { Injectable } 	from '@angular/core';
import { Http,
         Response}		from '@angular/http';

import { Observable }   from 'rxjs';

import { Hero }			from '../model/hero';

import 'rxjs/add/operator/map';

@Injectable()
export class HeroService {
	constructor(private http: Http) { }


  getHeroes(): Observable<[Hero]> {
 		return this.http.get('https://demo0385786.mockable.io/hero/getAll')
 			.map((response: Response) => {
 			  return response.json();
  	});
 	};
}