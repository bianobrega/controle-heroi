import { Injectable } 		from '@angular/core';
import { Http,
         Response}				from '@angular/http';

import { Observable }   	from 'rxjs';

import { Chart }					from '../model/chart';
import { StatisticUtil }	from '../helper/statistic.helper'

import 'rxjs/add/operator/map';

@Injectable()
export class StatisticService {
	constructor(private http: Http, private statisticUtil:StatisticUtil) { }


  getGeneralDataChart(typeChart:number): Observable<Chart> {
  	console.log(localStorage.getItem('currentStatistic'));
  	//Necessário pois como a API é mockada ela não retorna valores diferentes, 
  	//por isso vamos chamar várias APIs diferentes.
  	var url = this.statisticUtil.
  		getUrlStatistic(parseInt(sessionStorage.getItem('currentStatistic')), typeChart);

 		return this.http.get(url)
 			.map((response: Response) => {
 			  return response.json();
  	});
 	};
};
