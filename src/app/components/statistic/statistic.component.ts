import { Component,
        OnInit } from '@angular/core';

import { Chart }     from '../../model/chart'

@Component({
	selector: 'bar-chart-demo',
	templateUrl: './statistic.component.html'
})

export class StatisticComponent implements OnInit {
  showHeroStatistic:boolean;
  showGeneralStatistic:boolean;
  showPeopleStatistic:boolean;

  chart:Chart = new Chart();

  constructor() { }

  ngOnInit(): void {
    this.showHeroStatistic = false;
    this.showGeneralStatistic = false;
    this.showPeopleStatistic = false;
  }

  getStatistic(statisticType: number): void {
    if (statisticType === 1) {
      this.showHeroStatistic = true;
      this.showGeneralStatistic = false;
      this.showPeopleStatistic = false;

    } else if (statisticType === 2) {
      this.showHeroStatistic = false;
      this.showGeneralStatistic = true;
      this.showPeopleStatistic = false;

    } else if (statisticType === 3) {
      this.showHeroStatistic = false;
      this.showGeneralStatistic = false;
      this.showPeopleStatistic = true;
    }

    sessionStorage.setItem('currentStatistic', JSON.stringify(statisticType));
    
  }
};
