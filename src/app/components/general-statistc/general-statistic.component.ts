import { Component,
        OnInit,
        ViewChild }         from '@angular/core';
import { Chart }             from '../../model/chart';
import { StatisticService }  from '../../service/statistic.service';
import { BaseChartDirective } from 'ng2-charts/ng2-charts';
import { StatisticUtil }      from '../../helper/statistic.helper';

@Component({
  selector: 'general-statistic',
  templateUrl: './general-statistic.component.html',
  providers:[StatisticService, StatisticUtil]
})

export class GeneralStatisticComponent implements OnInit{

  @ViewChild( BaseChartDirective ) baseChart: BaseChartDirective;
  private updateChart(){
    if(this.baseChart) {
      this.baseChart.labels = this.chart.label;
      this.baseChart.ngOnChanges({});
    }
  }

  chart:Chart;
  lblTitle:String;
  load:boolean;

  constructor(private statisticService: StatisticService, private statiscUtil: StatisticUtil) { }

  ngOnInit(): void {
    this.getDataChart(1);
  }

  getDataChart(dataType:number) {
    this.load = true;
    var infoStatistic = this.statiscUtil
      .getStatisticInfo(parseInt(sessionStorage.getItem('currentStatistic')), dataType);

      console.log(infoStatistic);

    this.lblTitle = infoStatistic[1];

    this.statisticService.getGeneralDataChart(dataType)
      .subscribe((chart: Chart) => {
        this.chart = new Chart();
        this.chart = chart;
        this.chart.type = infoStatistic[0];
        this.load = false;
        this.updateChart();
      }, (error) => {
        this.load = false;
        console.log(error);
      })
  }

};
