import { Component,
		OnInit } 			from '@angular/core'
import { Router }        	from '@angular/router';

import { UserService } 		from '../../service/user.service';
import { User }						from '../../model/user';

@Component({
	selector: 'login',
	templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit {
	login:User;
	submitErrorMessages: string[];
	submitted:boolean=false;

	constructor(private userService: UserService, private router: Router) { }

	ngOnInit(): void {
		this.login = new User();
		this.submitErrorMessages = [];
		this.submitted = false;
	}

	submitForm() {
		this.submitted = true;
		if (this.login.email === 'admin@teste.com'
				&& this.login.password === '123456') {
			this.login.role = 'A';
		
		} else if (this.login.email === 'hero@teste.com'
				&& this.login.password === '123456') {
			this.login.role = 'H';
			

		} else {
			//Forçar a chamada de um serviço sem sucesso no service para testes
			this.login.role = 'E';
		}

		this.userService.login(this.login)
			.subscribe((result: boolean) => {
				if (result) {
					window.location.reload();
					this.router.navigate(['/heroes']);
				} else {
					this.submitted =false;
					console.log('ERRO');
					this.submitErrorMessages.push("Estamos com um problema temporário, por favor, tente novamente mais tarde.");

				}
			}, (error) => {
				this.submitted =false;
				this.submitErrorMessages.push(JSON.parse(error['_body']).error);
				}
			);
	};
};
