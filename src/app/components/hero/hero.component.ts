import { Component, OnInit }      from '@angular/core';

import { Hero }                   from '../../model/hero';
import { HeroService }            from '../../service/hero.service';
import { UserService }            from '../../service/user.service';

@Component({
  providers: [HeroService],
	selector: 'hero',
	templateUrl: './hero.component.html'
})

export class HeroComponent implements OnInit{

	heroes:[Hero];
  newHero:Hero;
  showUpdateHero:boolean;
  currentHero:Hero;
  load = true;
  role:String;

  private heroDefault:Hero;


  constructor(private heroService: HeroService, private userService: UserService) { }

  ngOnInit(): void {
    this.newHero = new Hero();
    this.showUpdateHero = false;
    this.currentHero = new Hero ();
    this.getHeroes();
    this.role = this.userService.getLoggedUser().role;
  }

  getHeroes() {
    this.heroService.getHeroes()
      .subscribe((heroes: [Hero]) => {
        this.heroes = heroes;
        this.load = false;
        console.log(this.heroes)
      }, (error) => {
        this.load = false;
        console.log(error);
      })
  }

  editHero(hero:Hero) {
    this.currentHero = hero;

    this.heroDefault = new Hero();
    this.heroDefault.id = hero.id;
    this.heroDefault.name = hero.name;
    this.heroDefault.heroNumber = hero.heroNumber;
    this.heroDefault.avatar = hero.avatar;

    this.showUpdateHero = true;
  }

  updateHero() {
    //Chamar serviço para atualizar herói
    this.showUpdateHero = false;
  }

  cancelUpdateHero() {
    this.currentHero.name = this.heroDefault.name;
    this.currentHero.heroNumber = this.heroDefault.heroNumber;
    this.currentHero.avatar = this.heroDefault.avatar;
    this.showUpdateHero = false;
  }

  addHero() {
    this.heroes.push(this.newHero);

  }

  removeHero(hero: Hero) {
    this.heroes.splice(this.heroes.indexOf(hero), 1);  
  }
};
