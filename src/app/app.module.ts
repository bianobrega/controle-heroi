import { NgModule }                  from '@angular/core';
import { BrowserModule }             from '@angular/platform-browser';
import { ChartsModule }              from 'ng2-charts/';
import { FormsModule }               from '@angular/forms';
import { HttpModule }                from '@angular/http';

import { AppRoutingModule }          from './app.routes.mudule';

import { UserService }               from './service/user.service';

import { AppComponent }              from './app.component';
import { LoginComponent }            from './components/login/login.component'
import { DefaultHeaderComponent }    from './templates/default/header/header.component';
import { DefaultFooterComponent }    from './templates/default/footer/footer.component';
import { StatisticComponent }        from './components/statistic/statistic.component';
import { HeroComponent }             from './components/hero/hero.component';
import { GeneralErrorComponent }     from './templates/default/forms/general-error.component';
import { GeneralStatisticComponent } from './components/general-statistc/general-statistic.component';

@NgModule({
  imports: [ 
  	BrowserModule , 
  	AppRoutingModule,
    ChartsModule,
    FormsModule,
    HttpModule
  ],
  declarations: [ 
  	AppComponent,
  	LoginComponent,
  	DefaultHeaderComponent,
  	DefaultFooterComponent,
    StatisticComponent,
    HeroComponent,
    GeneralErrorComponent,
    GeneralStatisticComponent
  ],
  providers: [
    UserService
  ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }