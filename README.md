Projeto Controle de Heróis
==============================

Código fonte da aplicação.

Tecnologias utilizadas
-------------

Esta aplicação em uma camada web utilizando __Angular 2__.

Configuração
-------------

__Pré requisitos:__
Para o desenvolvimento neste projeto é necessário ter os seguintes itens instalados e configurados:

* [NodeJS](https://nodejs.org/);
* [Git](https://git-scm.com/);
* [TortoiseGit](https://tortoisegit.org/) *opcional;
* [Atom](https://atom.io/) *ou qualquer outro editor de textos avançado;

__Iniciando o projeto__

* Clone o repositório do Git na pasta desejada utilizando o comando `git clone https://SEU_USUARIO@bitbucket.org/Biamoura/controle-heroi.git`;
* Para cada um dos projetos, identificados por um arquivo *package.json* dentro de suas pastas, execute o comando `npm install`
* Para iniciar a camada Web execute o comando `npm start` em sua respectiva pasta

Login
-------------
Para realizar o login com um usuário administrador, utilizar o *admin@teste.com* e a senha: *123456*.

Para realizar o login com um usuário herói, utilizar o e-mail: *hero@teste.com* e a senha: *123456*.

O usuário herói não deve poder incluir, alterar ou excluir heróis.